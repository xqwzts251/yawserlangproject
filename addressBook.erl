%%%-------------------------------------------------------------------
%%% @author Konrad
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. lis 2014 15:45
%%%-------------------------------------------------------------------
-module(addressBook).
-author("Konrad").

%% API
-export([createAddressBook/0, addContact/3, addPhone/4, addEmail/4, removeContact/3, removeEmail/2, removePhone/2, getEmails/3,
getPhones/3, findByEmail/2,  findByPhone/2, exportToCSV/2,  getContact/3, doesContactExist/3, doesPhoneExist/2, giveMe/4, doesEmailExist/2
]).
-record(address_book, {name_surname, phones, mails}).

createAddressBook() -> [].

addContact(Book, Name, Surname) -> case doesContactExist(Book, Name, Surname) of
                                    false -> Book ++ [#address_book{name_surname = {Name, Surname}, phones = [], mails = []}] ;
                                    true -> forbidden
                                    end.


addPhone(Book, Name, Surname, Phone) ->  case doesPhoneExist(Book, Phone) of
                                           true -> forbidden;
                                           false ->
                                             Contact = getContact(Book, Name, Surname),
                                             case Contact of
                                             empty -> BookNew =  addContact(Book, Name, Surname),
                                               addPhone(BookNew, Name, Surname, Phone);
                                             _ ->  NewContact = Contact#address_book{phones = lists:append(Contact#address_book.phones,[Phone])},
                                             [X || X <- Book, X#address_book.name_surname =/= {Name, Surname}] ++ [NewContact]
                                             end
                                           end.



addEmail(Book, Name, Surname, Email) -> case doesEmailExist(Book, Email) of
                                          true -> forbidden;
                                          false ->
                                            Contact = getContact(Book, Name, Surname),
                                            case Contact of
                                            empty -> BookNew =  addContact(Book, Name, Surname),
                                                    addEmail(BookNew, Name, Surname, Email);
                                             _ ->NewContact = Contact#address_book{mails = lists:append(Contact#address_book.mails,[Email])},
                                            [X || X <- Book, X#address_book.name_surname =/= {Name, Surname}] ++ [NewContact]
                                            end
                                        end .

removeContact(Book, Name, Surname) -> lists:filter(fun(X) -> X#address_book.name_surname =/= {Name, Surname} end, Book) .

removeEmail(Book, Email) -> lists:map(fun(X) -> Tmp = X,
                                      Tmp#address_book{mails = X#address_book.mails -- [Email]} end ,Book) .

removePhone(Book, Phone) -> lists:map(fun(X) -> Tmp = X,
                                      Tmp#address_book{phones = X#address_book.phones -- [Phone]} end ,Book) .

getEmails(Book, Name, Surname) -> Contact = getContact(Book, Name, Surname),
                                  case Contact of
                                    empty -> [];
                                    _ -> Contact#address_book.mails
                                  end.


getPhones(Book, Name, Surname) -> Contact = getContact(Book, Name, Surname),
                                  case Contact of
                                    empty -> [];
                                    _ -> Contact#address_book.phones
                                  end.
%%


findByEmail(Book, Email) -> lists:filter(fun(X) -> lists:any(fun(Y) -> Y == Email end, X#address_book.mails )end,Book) .

%%
findByPhone(Book, Phone) -> lists:filter(fun(X) -> lists:any(fun(Y) -> Y == Phone end, X#address_book.phones )end,Book) .


exportToCSV([A], Filename) ->
                            {Name, Surname} = A#address_book.name_surname,
                            file:write_file(Filename,io_lib:fwrite("~p;~p;~p;~p;~n" ,[Name, Surname, A#address_book.phones,
                                                                              A#address_book.mails]), [append]);

exportToCSV([H | T], Filename) ->
                                {Name, Surname} = H#address_book.name_surname,
                                file:write_file(Filename,io_lib:fwrite("~p;~p;~p;~p;~n" ,[Name, Surname , H#address_book.phones,
                                                  H#address_book.mails]), [append]),
                                 exportToCSV(T, Filename).

%% FUNKCJE WALIDUJĄCE %%

%% KONTAKTY
doesContactExist([], Name, Surname) -> false;

doesContactExist([#address_book{name_surname = {Name, Surname}} | T], Name, Surname) -> true;

doesContactExist([#address_book{name_surname = {_, _}} | T], Name, Surname) -> doesContactExist(T, Name, Surname).

%%TELEFONY

doesPhoneExist(Book, Phone) -> lists:any(fun(X) -> lists:any(fun(Y) -> Y == Phone end,X#address_book.phones) end, Book).
%%

%%MAILE
doesEmailExist(Book, Email) -> lists:any(fun(X) -> lists:any(fun(Y) -> Y == Email end,X#address_book.mails) end, Book).
%%

%% Kontakt

getContact(Book, Name, Surname) -> Record = lists:filter(fun(X) -> X#address_book.name_surname == {Name, Surname} end, Book),
                                   case Record of
                                     [] -> empty;
                                     [A] -> A
                                   end.

%%Funkcje sprawdzające (coby nie było)
giveMe(Name, Surname, Phones, Mails) -> #address_book{name_surname = {Name, Surname}, phones = Phones, mails = Mails}.

isTupleImmutable(A, PhoneNew) -> A#address_book.phones ++ [PhoneNew].



